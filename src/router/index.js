import Vue from 'vue'
import Router from 'vue-router'
import Universe from "../components/Universe"
import CelestialBodies from "../components/CelestialBodies"
import CelestialBody from "../components/CelestialBody"
import Favorites from "../components/Favorites"

Vue.use(Router)

export default new Router({
    routes: [
        {path: '/', name: 'CelestialBodies', component: CelestialBodies},
        {path: '/celestial-body/:id', name: 'CelestialBody', component: CelestialBody},
        {path: '/favorites', name: 'Favorites', component: Favorites},
        {path: '/solar-system', name: 'Universe', component: Universe},
    ],
    mode: 'history'
})
