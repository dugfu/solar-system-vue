import Vue from 'vue'
import Vuex from 'vuex'
import favorites from './modules/favorites.store'
import fetchBodies from './modules/fetch-bodies.store'
import filter from './modules/filter.store'

Vue.use(Vuex)

export const store = new Vuex.Store({
  modules: {
      favorites,
      fetchBodies,
      filter
   }
})
