// State object
const state = {
  filterState: null
}

// Getter functions
const getters = {
  getFilter: state => state.filterState
}

// Actions
const actions = {
  setFilterAction (context, params) {
    context.commit('SET_FILTER', params)
  }
}

// Mutations
const mutations = {
  SET_FILTER(state, params) {
    state.filterState = params.filter
  }
}
export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
