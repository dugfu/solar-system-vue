import axios from 'axios'

// State object
const state = {
  bodiesState: []
}

// Getter functions
const getters = {
  getBodies: state => state.bodiesState
}

// Actions
const actions = {
  fetchBodiesAction (context) {
    if (state.bodiesState.length > 0) {
      return
    }
    axios.get('https://api.le-systeme-solaire.net/rest/bodies/')
    .then(response => {
      context.commit('FETCH_BODIES', response.data)
    })
  }
}

// Mutations
const mutations = {
  FETCH_BODIES(state, data) {
    state.bodiesState = data.bodies
  }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
