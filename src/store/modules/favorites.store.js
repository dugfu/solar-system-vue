// State object
const state = {
  favoritesState: []
}

// Getter functions
const getters = {
  getFavorites: state => state.favoritesState
}

// Actions
const actions = {
  addFavoriteAction (context, params) {
    context.commit('ADD_FAVORITE', params)
  },
  removeFavoriteAction (context, params) {
    context.commit('REMOVE_FAVORITE', params)
  }
}

// Mutations
const mutations = {
  ADD_FAVORITE(state, params) {
    state.favoritesState.push(params.body)
  },
  REMOVE_FAVORITE(state, params) {
    state.favoritesState.splice(params.index, 1)
  }
}
export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
